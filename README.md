# Safe - Issue tracker

Issue tracker for Safe plugin (Matherion.eu)

# Labels

Označujte prosím chyby vhodnými labely + důležitostí opravy.

# K otestování

permissions:
- matherion.safe.open:
    - description: Allows /safe command
-  matherion.safe.open.others:
    - description: Allows usage of /safe <player> to view contents of other players safes
-  matherion.safe.open.others.modify:
    - description: Allows modifying contents of other players safes
-  matherion.safe.rows.one:
    - description: Player will have one row in Safe inventory (higher number overrides)
-  matherion.safe.rows.two:
    - description: Player will have two rows in Safe inventory (higher number overrides)
-  matherion.safe.rows.three:
    - description: Player will have three rows in Safe inventory (higher number overrides)
-  matherion.safe.rows.four:
    - description: Player will have four rows in Safe inventory (higher number overrides)
-  matherion.safe.rows.five:
    - description: Player will have five rows in Safe inventory (higher number overrides)
-  matherion.safe.rows.six:
    - description: Player will have six rows in Safe inventory (higher number overrides)
-  matherion.safe.reload:
    - description: Allow reloading of configuration

NBT / Custom name / Lore / Enchanty - persistence
